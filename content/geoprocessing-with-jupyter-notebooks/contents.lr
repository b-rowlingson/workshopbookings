_model: workshop
---
title: Geoprocessing with Jupyter Notebooks
---
affiliation: SCISYS
---
description:

The Jupyter notebook is becoming the de-facto environment for data scientists 
using the so-called "pydata stack" (python packages of numpy, pandas, matplotlib, other). Now, there is increasing 
crossover between desktop GIS specialists and python developers, as python packages that manipulate geographic 
data mature. Notebooks are a powerful yet easy to use environment to write code with and allow you to interact 
with your data and share workflows easily.

The objectives of this short workshop are to provide GIS users with a hands-on introduction to (a) how to set up a 
jupyter notebook environment locally using Anaconda and install various required python packages (b) undertake 
some simple geo-processing tasks using a mixture of geo packages within the pydata ecosystem, e.g. rasterio, 
xarray and geopandas, (c) visualise processing outputs.

 Code, data, and instructions to build your environment are all available in the repo [here](https://github.com/samfranklin/foss4guk19-jupyter/).
---
prereqs:

The workshop is aimed at those with a GIS background with "beginner-level" awareness of python but have not used Jupyter notebooks and want to learn more. If you're already familiar with “geo” python packages and notebooks, please feel free to still come along as you may pick up something new.

Bring a laptop

Download the [entire workshop repository](https://github.com/samfranklin/foss4guk19-jupyter) beforehand.

Have a go at [setting up your environment](https://github.com/samfranklin/foss4guk19-jupyter/blob/master/README.md#how-to-build-your-environment)

Don’t worry though, we will run through this at the beginning of the workshop too.
---
presenter: Sam Franklin
